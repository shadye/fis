/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React from 'react';
import Router from 'react-routing/src/Router';
import http from './core/http';
import App from './components/App';
import ContentPage from './pages/ContentPage';
import ContactPage from './pages/ContactPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import NotFoundPage from './pages/NotFoundPage';
import ErrorPage from './pages/ErrorPage';
import Index from './pages/Index';
import AuthStore from './stores/AuthStore';
import location from './core/Location';

const router = new Router(on => {

  function auth(component) {
    if (!!AuthStore.jwt) {
      return component;
    } else {
      location.navigateTo('/');
      return <Index />;
    }
  };

  on('*', async (state, next) => {
    const component = await next();
    return component && <App context={state.context}>{component}</App>;
  });

  on('/', async () => <Index />);

  on('/contact', async () => auth(<ContactPage />));

  on('/login', async () => auth(<LoginPage />));

  on('/register', async () => <RegisterPage />);

  /*on('*', async (state) => {
    const content = await http.get(`/api/content?path=${state.path}`);
    return content && <ContentPage {...content} />;
  });*/

  on('error', (state, error) => state.statusCode === 404 ?
    <App context={state.context} error={error}><NotFoundPage /></App> :
    <App context={state.context} error={error}><ErrorPage /></App>);

});

export default router;
