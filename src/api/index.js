import { Router } from 'express';
import content from './content';
import user from './user';

const router = new Router();
router.get('/', content.get);

router.post('/user/create', user.create);
router.post('/user/get', user.get);

export default router;
