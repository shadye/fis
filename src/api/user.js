import _ from 'lodash';
import jwt from 'jsonwebtoken';
import mongoose, {Schema} from 'mongoose';

const db = global.server.db;

let userSchema = new Schema({
  name: String,
  surname: String,
  male: Boolean,
  login: String,
  tutor: Boolean,
  student: Boolean,
  email: String,
  password: String
});

let User = mongoose.model('User', userSchema);

// let me = new User({
//   name: 'Алексей',
//   surname: 'Судавцов',
//   male: true,
//   login: 'shadye',
//   tutor: false,
//   email: 'shadye@ya.ru',
//   password: 'ybrjnby'
// });
//
// me.save((error)=>{
//   console.error(error);
// })

let users = [{
  id: 1,
  username: 'shadye',
  password: 'shadye'
}];

let secret = 'fis is awesome';

function createToken(user) {
  return jwt.sign(_.omit(user, 'password'), secret, { expiresInMinutes: 60*5 });
}

const user = {};

user.get = async (req, res, next) => {
  try {
    if (!req.body.username || !req.body.password) {
      return res.status(400).send("You must send the username and the password");
    }

    // var user = _.find(users, {username: req.body.username});
    let user = await User.findOne({login: req.body.username, password: req.body.password});

    if (!user) {
      return res.status(401).send("The username or password don't match");
    }

    // if (user.password !== req.body.password) {
    //   return res.status(401).send("The username or password don't match");
    // }

    res.status(201).send({
      id_token: createToken(user)
    });
  } catch (err) {
    next(err);
  }
};

user.create = async (req, res, next) => {
  let b = req.body;
  let me = new User({
    fio: b.name,
    gender: b.gender,
    login: b.login,
    tutor: b.tutor,
    student: b.student,
    email: b.email,
    password: b.password
  });

  me.save((error)=>{
    console.error(error);
  });

  res.status(201).send("Успешно");
};

export default user;
