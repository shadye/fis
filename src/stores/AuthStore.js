import alt from '../core/Alt';
import AuthActions from '../actions/AuthActions'
import jwt_decode from 'jwt-decode';
import localStorage from 'localStorage';

class AuthStore {

  constructor() {
    this.bindActions(AuthActions);
    let jwt = localStorage.getItem('jwt');
    if (jwt) {
      this.jwt = jwt;
      this.user = jwt_decode(this.jwt);
    }
  }

  login(jwt) {
    this.jwt = jwt;
    this.user = jwt_decode(this.jwt);
  }

  logout() {
    this.jwt = null;
    this.user = null;
    console.log('logout');
  }

  // _registerToActions(action) {
  //   switch(action.actionType) {
  //     case LOGIN_USER:
  //       this._jwt = action.jwt;
  //       this._user = jwt_decode(this._jwt);
  //       this.emitChange();
  //       break;
  //     case LOGOUT_USER:
  //       this._user = null;
  //       this.emitChange();
  //       console.log("logout");
  //       break;
  //     default:
  //       break;
  //   }
  // }
  //
  // get user() {
  //   return this._user;
  // }
  //
  // get jwt() {
  //   return this._jwt;
  // }
  //
  // isLoggedIn() {
  //   return !!this._user;
  // }
}

export default alt.createStore(AuthStore);
