import alt from '../core/Alt';
import LanguageActions from '../actions/LanguageActions'
import ru from '../content/ru';
import en from '../content/en';

class LanguageStore {

  constructor() {
    this.bindActions(LanguageActions);
    this.content = ru;
  }

  update(lang) {
    switch (lang) {
      case "ru":
        this.content = ru;
        break;
      case "en":
        this.content = en;
        break;
      default:
        break;
    }
  }
}

export default alt.createStore(LanguageStore);
