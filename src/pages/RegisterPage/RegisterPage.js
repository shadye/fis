/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes } from 'react';
import withStyles from '../../decorators/withStyles';
import styles from './RegisterPage.css';
import Wrapper from '../../components/ComponentWrapper';
import sc from '../../constants/Suggestions';
import Captcha from '../../components/Captcha';
import AuthService from '../../services/AuthService';

@withStyles(styles)
class RegisterPage extends Wrapper {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    $("#name").suggestions({
      serviceUrl: "https://dadata.ru/api/v2",
      token: sc.TOKEN,
      type: "NAME"
    });
    $("#address").suggestions({
      serviceUrl: "https://dadata.ru/api/v2",
      token: sc.TOKEN,
      type: "ADDRESS"
    });
    $("#email").suggestions({
      serviceUrl: "https://dadata.ru/api/v2",
      token: sc.TOKEN,
      type: "EMAIL"
      // onSelect: function(suggestion) {
      //   console.log(suggestion);
      // }
    });
  }

  prepare(inputs) {
    console.log(inputs);
    let values = {};
    for (let i = 1; i <= inputs.length-1; i++) {
      let input = inputs[i];
      switch (input.id) {
        case "male":
          if (input.checked) {
            values.gender = true;
          } else {
            break;
          }
          break;
        case "female":
          if (input.checked) {
            values.gender = false;
          } else {
            break;
          }
          break;
        case "student":
        case "tutor":
          if (input.checked) {
            values[input.id] = true;
          } else {
            values[input.id] = false;
          }
          break;
        case "passwordAgain":
          if (input.value === values.password) {
            break;
          } else {
            alert("Пароли не совпадают.");
            return;
          }
          break;
        case "g-recaptcha-response":
          if(input.value.length) {
            break;
          } else {
            alert("Подтведите, что вы - человек.");
            return;
          }
          break;
        default:
          if (input.validity.valid) {
            values[input.id] = input.value;
          } else {
            input.reportValidity();
            return;
          }
          break;
      }
    }
    AuthService.signup(values);
    console.log(values);
  }

  onSubmit(e) {
    e.preventDefault();
    this.prepare(e.target.form.elements);
  }

  render() {

    const content = this.state.content.register;
    const ci = content.inputs;
    const me = this;

    return (
      <div className="RegisterPage">
        <h1 className="RegisterPage-header">{content.header}</h1>
        <div className="RegisterPage-container">
          <form className="pure-form pure-form-aligned">
            <fieldset>
                
                <div className="pure-control-group RegisterPage-field">
                    <label for="name">{ci.name.label}</label>
                    <input 
                      className="RegisterPage-input" 
                      id="name" 
                      type="text" 
                      pattern={ci.name.pattern} 
                      placeholder={ci.name.placeholder} 
                      required/>
                </div>

                <div className="pure-control-group RegisterPage-field">
                    <label>{ci.gender.label}</label>
                    <div className="RegisterPage-cr-container">
                      <input className="RegisterPage-input_cr" id="male" type="radio" name="gender" value={ci.gender.values[0]} checked/>
                      <label for="male" className="pure-radio RegisterPage-cr-label">{ci.gender.values[0]}</label>
                      <input className="RegisterPage-input_cr" id="female" type="radio" name="gender" value={ci.gender.values[1]}/>
                      <label for="female" className="pure-radio RegisterPage-cr-label">{ci.gender.values[1]}</label>
                    </div>
                </div>

                <div className="pure-control-group RegisterPage-field">
                    <label for="address">{ci.address.label}</label>
                    <input 
                      className="RegisterPage-input" 
                      id="address" 
                      type="text" 
                      pattern={ci.address.pattern} 
                      placeholder={ci.address.placeholder} 
                      required/>
                </div>

                <div className="pure-control-group RegisterPage-field">
                    <label for="login">{ci.login.label}</label>
                    <input 
                      className="RegisterPage-input" 
                      id="login" 
                      type="text" 
                      pattern={ci.login.pattern} 
                      placeholder={ci.login.placeholder} 
                      required/>
                </div>

                <div className="pure-control-group RegisterPage-field">
                  <label>{ci.status.label}</label>
                  <div className="RegisterPage-cr-container">
                    <input className="RegisterPage-input_cr" id="student" type="checkbox" name="status" value={ci.status.values[0]}/>
                    <label for="female" className="pure-checkbox RegisterPage-cr-label">{ci.status.values[1]}</label>
                    <input className="RegisterPage-input_cr" id="tutor" type="checkbox" name="status" value={ci.status.values[0]}/>
                    <label for="male" className="pure-checkbox RegisterPage-cr-label">{ci.status.values[0]}</label>
                  </div>
                </div>

                <div className="pure-control-group RegisterPage-field">
                    <label for="email">{ci.email.label}</label>
                    <input 
                      className="RegisterPage-input" 
                      id="email" 
                      type="email" 
                      pattern={ci.email.pattern} 
                      placeholder={ci.email.placeholder} 
                      required/>
                </div>

                <div className="pure-control-group RegisterPage-field">
                    <label for="password">{ci.password.label}</label>
                    <input 
                      className="RegisterPage-input" 
                      id="password" 
                      type="password" 
                      pattern={ci.password.pattern} 
                      placeholder={ci.password.placeholder} 
                      required/>
                </div>

                <div className="pure-control-group RegisterPage-field">
                    <label for="passwordAgain">{ci.passwordAgain.label}</label>
                    <input 
                      className="RegisterPage-input" 
                      id="passwordAgain" 
                      type="password" 
                      pattern={ci.passwordAgain.pattern} 
                      placeholder={ci.passwordAgain.placeholder} 
                      required/>
                </div>

                <Captcha className="RegisterPage-captcha"/>

                <div className="pure-controls">
                    <button type="submit" className="pure-button pure-button-primary RegisterPage-submit" onClick={me.onSubmit.bind(me)}>{content.submit}</button>
                </div>
            </fieldset>
          </form>
        </div>
      </div>
    );
  }

  

}

export default RegisterPage;
