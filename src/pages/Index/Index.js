import React, { PropTypes } from 'react';
import styles from './Index.css';
import withStyles from '../../decorators/withStyles';
import Carousel from '../../components/Carousel';
import Teasers from '../../components/Teasers';
import Sections from '../../components/Sections';


@withStyles(styles)
class Index extends React.Component {

  constructor() {
    super();
  }

  render() {
    return (
      <div className="IndexPage">
        <Carousel className="IndexPage-carousel"/>
        <div className="IndexPage-container">
          <Teasers className="IndexPage-teasers"/>
        </div>
        <Sections className="IndexPage-sections"/>
      </div>
    )
  }
}

export default Index;
