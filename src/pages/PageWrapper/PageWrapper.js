import React from 'react';
import connectToStores from 'alt/utils/connectToStores';
import LanguageStore from '../../stores/LanguageStore';

class PageWrapper extends React.Component {

  // constructor() {
  //   super();
  //   this.state = LanguageStore.getState();
  // }

  // componentDidMount() {
  //   LanguageStore.listen(this.onLanguageChange.bind(this));
  // }
  //
  // componentWillUnmount() {
  //   LanguageStore.unlisten(this.onLanguageChange.bind(this));
  // }
  //
  // onLanguageChange(state) {
  //   // this.setState({content: state.content});
  //   this.state.content = state.content;
  //   console.log(1);
  // }

  static getStores() {
    return [LanguageStore];
  }

  static getPropsFromStores() {
    return LanguageStore.getState();
  }

}

export default connectToStores(PageWrapper);
