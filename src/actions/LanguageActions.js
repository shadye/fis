import alt from '../core/Alt';

class LanguageActions {
  update(language) {
    this.dispatch(language);
    console.log('changed');
  }
}

export default alt.createActions(LanguageActions);
