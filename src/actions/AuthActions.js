import alt from '../core/Alt';
import localStorage from 'localStorage';

class AuthActions {

  login(jwt) {
    let savedJwt = localStorage.getItem('jwt');

    if (savedJwt != jwt) {
      localStorage.setItem('jwt', jwt);
    }
    this.dispatch(jwt);
  }

  logout() {
    localStorage.removeItem('jwt');
    this.dispatch();
  }
}

export default alt.createActions(AuthActions);

// export default {
//   loginUser: (jwt) => {
//     let savedJwt = localStorage.getItem('jwt');
//
//     if (savedJwt != jwt) {
//       location.navigateTo('/');
//       localStorage.setItem('jwt', jwt);
//     }
//
//     AppDispatcher.dispatch({
//       actionType: LOGIN_USER,
//       jwt: jwt
//     });
//   },
//   logoutUser: () => {
//     location.navigateTo('/login');
//     localStorage.removeItem('jwt');
//     AppDispatcher.dispatch({
//       actionType: LOGOUT_USER
//     });
//   }
// }
