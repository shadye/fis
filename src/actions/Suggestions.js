import alt from '../core/Alt';

class SuggestionsAction {
  update(params) {
    this.dispatch(params);
  }
}

export default alt.createActions(SuggestionsAction);
