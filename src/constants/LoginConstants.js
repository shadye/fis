var BASE_URL = '/api/';
export default {
  BASE_URL: BASE_URL,
  LOGIN_URL: BASE_URL + 'user/get',
  SIGNUP_URL: BASE_URL + 'user/create',
  LOGIN_USER: 'LOGIN_USER',
  LOGOUT_USER: 'LOGOUT_USER'
}
