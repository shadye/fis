import request from 'reqwest';
import when from 'when';
import sc from '../constants/Suggestions';
import sa from '../actions/Suggestions';

class SuggestionsService {

  get(value, type, list) {
    let data = JSON.stringify({query: value});
    return this.returnSuggest(when(request({
      url: sc[type],
      method: "POST",
      crossOrigin: true,
      type: 'json',
      contentType: 'application/json',
      headers: {
        'Authorization': sc.TOKEN,
        'Accept': 'application/json',
        'X-Secret': sc.SECRET
      },
      data: {
        'query': value
      }
    })), list);
  }

  returnSuggest(suggestPromise, list) {
    return suggestPromise
      .then(function(response) {
         sa.update({list: list, suggestions: response.suggestions});
      });
  }

}

export default new SuggestionsService();
