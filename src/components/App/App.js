/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import styles from './App.css';
import withContext from '../../decorators/withContext';
import withStyles from '../../decorators/withStyles';
import Header from '../Header';
import Feedback from '../Feedback';
import Footer from '../Footer';

@withContext
@withStyles(styles)
class App extends Component {

  static propTypes = {
    children: PropTypes.element.isRequired,
    error: PropTypes.object
  }

  constructor() {
    super();
  }

  // componentDidMount() {
  //   LanguageStore.listen(this.onLanguageChange.bind(this));
  // }
  //
  // componentWillUnmount() {
  //   LanguageStore.unlisten(this.onLanguageChange.bind(this));
  // }
  //
  // onLanguageChange(state) {
  //   this.forceUpdate();
  //   console.log(1);
  // }

  render() {
    return !this.props.error ? (
      <div className="App">
        <div className="App-wrapper">
          <Header/>
          {this.props.children}
        </div>
        <Footer/>
      </div>
    ) : this.props.children;
  }

}

export default App;
