import React from 'react';
import Wrapper from '../ComponentWrapper';
import classNames from 'classnames';
import styles from './Teasers.css';
import withStyles from '../../decorators/withStyles';

@withStyles(styles)
class Teasers extends Wrapper {

  constructor(props) {
    super(props);
  }

  render() {
    const content = this.state.content.teasers;
    return(
      <div className={classNames(this.props.className, 'Teasers')}>
        <div className="Teasers-container">
          <h1 className="Teasers-logoQuestion">{content.logo}</h1>
          <div className="Teasers-teasersContainer">
            <div className="Teasers-teaser">
              <img className="Teasers-teaser-img" src={require('./monitor.png')} alt={content.education}></img>
              <p className="Teasers-teaser-logo">{content.education}</p>
            </div>
            <div className="Teasers-teaser">
              <img className="Teasers-teaser-img" src={require('./books.png')} alt={content.tests}></img>
              <p className="Teasers-teaser-logo">{content.tests}</p>
            </div>
            <div className="Teasers-teaser">
              <img className="Teasers-teaser-img" src={require('./airplan.png')} alt={content.training}></img>
              <p className="Teasers-teaser-logo">{content.training}</p>
            </div>
            <div className="Teasers-teaser">
              <img className="Teasers-teaser-img" src={require('./dialogs.png')} alt={content.dialogs}></img>
              <p className="Teasers-teaser-logo">{content.dialogs}</p>
            </div>
            <div className="Teasers-teaser">
              <img className="Teasers-teaser-img" src={require('./friends.png')} alt={content.friends}></img>
              <p className="Teasers-teaser-logo">{content.friends}</p>
            </div>
            <div className="Teasers-teaser">
              <img className="Teasers-teaser-img" src={require('./play.png')} alt={content.content}></img>
              <p className="Teasers-teaser-logo">{content.content}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }

}

export default Teasers;
