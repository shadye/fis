import React from 'react';
import classNames from 'classnames';
import styles from './Localize.css';
import withStyles from '../../decorators/withStyles';
import LanguageActions from '../../actions/LanguageActions';
import Arrows from '../Arrows';

@withStyles(styles)
class Localize extends React.Component {

  constructor(props) {
    super(props);
  }

  updateLanguage(e) {
    LanguageActions.update(e.target.value);
  }

  render() {
    let self = this;
    const select = (
      <select className="Localize-select" defaultValue="ru" onChange={this.updateLanguage}>
        <option value="ru">Русский</option>
        <option value="en">English</option>
      </select>
    );
    return (
      <div className={classNames(this.props.className, 'Localize')}>
        <div className="Localize-container">
          <Arrows className="Localize-arrows" insert={select}/>
        </div>
      </div>
    )
  }
}

export default Localize;
