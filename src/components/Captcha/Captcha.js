import React, { PropTypes } from 'react';
import classnames from 'classnames';

class Captcha extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    grecaptcha.render(this.refs.captcha.getDOMNode(), {
      sitekey: "6LdMHQ8TAAAAAErSEL7LRfarfR31im-2QXY9ZXxc"
    });
  }

  render() {
    return(
      <div id="captcha" ref="captcha" name="captcha" className={classnames(this.props.className, 'captcha')}></div>
    );
  }
}

export default Captcha;
