/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */
import React, {Component} from 'react';
import styles from './Footer.css';
import withStyles from '../../decorators/withStyles';
import Link from '../../utils/Link';
import classNames from 'classnames';
import FooterContent from '../FooterContent';

@withStyles(styles)
class Footer extends Component {

  constructor() {
    super();
    this.state={};
  }

  render() {
    return (
      <div className="Footer">
        <div className="Footer-container">
          <FooterContent className="Footer-content"></FooterContent >
        </div>
      </div>
    );
  }

}

export default Footer;
