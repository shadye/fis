import React, {Component} from 'react';
import Wrapper from '../ComponentWrapper';
import classNames from 'classnames';
import styles from './Logo.css';
import withStyles from '../../decorators/withStyles';

@withStyles(styles)
class Logo extends Wrapper {

  constructor(props) {
    super(props);
  }

  render() {
    const content = this.state.content.logo;
    return (
      <div className={classNames(this.props.className, 'Logo')}>
        <div className="Logo-container">
          <h1 className="Logo-name">fis</h1>
          <h2 className="Logo-description">{content.description}</h2>
        </div>
      </div>
    );
  }
}

export default Logo;
