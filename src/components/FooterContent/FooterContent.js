import React from 'react';
import Wrapper from '../ComponentWrapper';
import styles from './FooterContent.css';
import withStyles from '../../decorators/withStyles';
import Link from '../../utils/Link';
import classNames from 'classnames';

@withStyles(styles)
class FooterContent extends Wrapper {

  constructor(props) {
    super(props);
  }

  render() {
    const content = this.state.content.footer;
    return(
      <div className={classNames(this.props.className, 'FooterContent')}>
        <div className="FooterContent-links">
             <a className="FooterContent-link" href="/" onClick={Link.handleClick}>{content.links.main}</a>
             <span className="FooterContent-spacer">·</span>
             <a className="FooterContent-link" href="/" onClick={Link.handleClick}>{content.links.tutors}</a>
             <span className="FooterContent-spacer">·</span>
             <a className="FooterContent-link" href="/privacy" onClick={Link.handleClick}>{content.links.students}</a>
             <span className="FooterContent-spacer">·</span>
             <a className="FooterContent-link" href="/privacy" onClick={Link.handleClick}>{content.links.faq}</a>
             <span className="FooterContent-spacer">·</span>
             <a className="FooterContent-link" href="/privacy" onClick={Link.handleClick}>{content.links.rules}</a>
             <span className="FooterContent-spacer">·</span>
             <a className="FooterContent-link" href="/privacy" onClick={Link.handleClick}>{content.links.forum}</a>
         </div>
         <div className="FooterContent-copyright">
           <span className="FooterContent-text">{content.copyright}</span>
         </div>
         <div className="FooterContent-description">
           <span className="FooterContent-text">{content.description}</span>
         </div>
      </div>
    );
  }

}

export default FooterContent;
