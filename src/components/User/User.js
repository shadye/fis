import React from 'react';
import Wrapper from '../ComponentWrapper';
import classNames from 'classnames';
import styles from './User.css';
import withStyles from '../../decorators/withStyles';
import AuthStore from '../../stores/AuthStore';
import localStorage from 'localStorage';
import jwt_decode from 'jwt-decode';
import Arrows from '../Arrows';
import ContextMenu from '../ContextMenu';

@withStyles(styles)
class User extends Wrapper {

  constructor() {
    super();
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
    let jwt = localStorage.getItem('jwt');
    if (jwt) {
      this.state.user = jwt_decode(jwt);
    }
  }

  componentDidMount() {
    AuthStore.listen(this.onAuthStoreChange);
  }

  componentWillUnmount() {
    AuthStore.unlisten(this.onAuthStoreChange);
  }

  onAuthStoreChange(state) {
    this.setState(state);
    console.log(state);
  }



  render() {
    const content = this.state.content.user;
    const me = this;
    const name = <div className="User-name">{this.state.user.name}</div>;
    const menu = <ContextMenu className="User-contextMenu" width="50px"/>
    return(
      <div className={classNames(this.props.className, 'User')}>
        <div className="User-container">
          <p className="User-greeting">{content.greeting}</p>
          <Arrows className="User-arrows" insert={[name, menu]}/>
        </div>
        <div className="User-messages-container">
          <p className="User-messages">{content.messages}</p>
          <p className="User-messages-count">0</p>
        </div>
      </div>
    )
  }

}

export default User;
