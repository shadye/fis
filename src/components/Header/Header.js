/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, {Component} from 'react';
import styles from './Header.css';
import withStyles from '../../decorators/withStyles';
import Link from '../../utils/Link';
import Login from '../Login';
import User from '../User';
import Logo from '../Logo';
import Social from '../Social';
import Localize from '../Localize';
import Menu from '../Menu';
import AuthStore from '../../stores/AuthStore';

@withStyles(styles)
class Header extends Component {

  constructor(props) {
    super(props);
    this.onAuthChange = this.onAuthChange.bind(this);
    this.state = AuthStore.getState();
  }

  componentDidMount() {
    AuthStore.listen(this.onAuthChange);
  }

  componentWillUnmount() {
    AuthStore.unlisten(this.onAuthChange);
  }

  onAuthChange() {
    this.setState(AuthStore.getState());
  }

  render() {
    let loginOrUser = null;

    if (this.state.user) {
      loginOrUser = <User className="Header-user"></User>;
    } else {
      loginOrUser = <Login className="Header-login"></Login>;
    }

    return (
      <div className="Header">
        <div className="Header-container">
          <Logo className="Header-logo"></Logo>
          {loginOrUser}
          <Social className="Header-social"></Social>
          <Localize className="Header-localize"></Localize>
          {/*<a className="Header-brand" href="/" onClick={Link.handleClick}>
            <img className="Header-brandImg" src={require('./logo-small.png')} width="38" height="38" alt="React" />
            <span className="Header-brandTxt">Your Company</span>
          </a>*/}
          {/*<div className="Header-banner">
            <h1 className="Header-bannerTitle">React</h1>
            <p className="Header-bannerDesc">Complex web apps made easy</p>
          </div>*/}
        </div>
        <Menu className="Header-menu"></Menu>
      </div>
    );
  }
}

export default Header;
