import React from 'react';
import Wrapper from '../ComponentWrapper';
import styles from './Social.css';
import classNames from 'classnames';
import withStyles from '../../decorators/withStyles';

@withStyles(styles)
class Social extends Wrapper {

  constructor(props) {
    super(props);
  }

  render() {
    const content = this.state.content.social;
    return (
      <div className={classNames(this.props.className, "Social")}>
        <div className="Social-container">
          <p className="Social-description">{content.description}</p>
          <div className="Social-linkContainer">
            <span className="fa-stack">
              <i className="fa fa-circle fa-stack-2x Social-circle"></i>
              <i className="fa fa-facebook fa-stack-1x Social-icon"></i>
            </span>
            <span className="fa-stack">
              <i className="fa fa-circle fa-stack-2x Social-circle"></i>
              <i className="fa fa-vk fa-stack-1x Social-icon"></i>
            </span>
            <span className="fa-stack">
              <i className="fa fa-circle fa-stack-2x Social-circle"></i>
              <i className="fa fa-youtube fa-stack-1x Social-icon"></i>
            </span>
          </div>
        </div>
      </div>
    )
  }
}

export default Social;
