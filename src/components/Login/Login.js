import React from 'react';
import Wrapper from '../ComponentWrapper';
import classNames from 'classnames';
import styles from './Login.css';
import withStyles from '../../decorators/withStyles';
import Link from '../../utils/Link';
import Navigation from '../Navigation';
import Auth from '../../services/AuthService';

@withStyles(styles)
class Login extends Wrapper {

  constructor(props) {
    super(props);
    this.setState({
      user: '',
      password: ''
    });
  }

  login(e) {
    e.preventDefault();
    Auth.login(this.state.user, this.state.password);
  }

  handleChange(event) {
    this.state[event.target.id] =  event.target.value;
  }

  render() {
    let self = this;
    const content = this.state.content.login;
    return (
      <div className={classNames(this.props.className, 'Login')}>
        <div className="Login-container">
          <form role="form">
            <input type="text" id="user" onChange={self.handleChange.bind(this)} className="Login-userField" placeholder={content.placeholders.user}/>
            <input type="password" id="password" onChange={self.handleChange.bind(this)} className="Login-passwordField" placeholder={content.placeholders.pass}/>
            <button className="Login-submitButton" type="submit" onClick={this.login.bind(this)}>></button>
          </form>
          <div className="Login-helpers">
            <a href="#" className="Login-helpers_forgotPass">{content.links.forgotPass}</a>
            <a href="/register" onClick={Link.handleClick} className="Login-helpers_registration">{content.links.registration}</a>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
