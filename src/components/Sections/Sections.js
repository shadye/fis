import React from 'react';
import Wrapper from '../ComponentWrapper';
import classNames from 'classnames';
import styles from './Sections.css';
import withStyles from '../../decorators/withStyles';

@withStyles(styles)
class Sections extends Wrapper {

  constructor(props) {
    super(props);
  }

  render() {
    const content = this.state.content.sections;
    return(
      <div className={classNames(this.props.className, 'Sections')}>
        <div className="Sections-container">
          <h1 className="Sections-logo">{content.logo}</h1>
          <div className="Sections-sectionsContainer">
            <div className="Sections-section dark-green">
              <p className="Sections-section-logo">{content.section.logo}</p>
              <p className="Sections-section-description">{content.section.description}</p>
            </div>
            <div className="Sections-section green">
              <p className="Sections-section-logo">{content.section.logo}</p>
              <p className="Sections-section-description">{content.section.description}</p>
            </div>
            <div className="Sections-section light-blue">
              <p className="Sections-section-logo">{content.section.logo}</p>
              <p className="Sections-section-description">{content.section.description}</p>
            </div>
            <div className="Sections-section light-violet">
              <p className="Sections-section-logo">{content.section.logo}</p>
              <p className="Sections-section-description">{content.section.description}</p>
            </div>
            <div className="Sections-section yellow">
              <p className="Sections-section-logo">{content.section.logo}</p>
              <p className="Sections-section-description">{content.section.description}</p>
            </div>
            <div className="Sections-section orange">
              <p className="Sections-section-logo">{content.section.logo}</p>
              <p className="Sections-section-description">{content.section.description}</p>
            </div>
            <div className="Sections-section violet">
              <p className="Sections-section-logo">{content.section.logo}</p>
              <p className="Sections-section-description">{content.section.description}</p>
            </div>
            <div className="Sections-section gray">
              <p className="Sections-section-logo">{content.section.logo}</p>
              <p className="Sections-section-description">{content.section.description}</p>
            </div>
            <div className="Sections-section blue">
              <p className="Sections-section-logo">{content.section.logo}</p>
              <p className="Sections-section-description">{content.section.description}</p>
            </div>
            <div className="Sections-section red">
              <p className="Sections-section-logo">{content.section.logo}</p>
              <p className="Sections-section-description">{content.section.description}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }

}

export default Sections;
