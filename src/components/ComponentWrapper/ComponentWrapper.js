import React from 'react';
import LanguageStore from '../../stores/LanguageStore';

class ComponentWrapper extends React.Component {

  constructor(props) {
    super(props);
    this.onLanguageChange = this.onLanguageChange.bind(this);
    this.state = LanguageStore.getState();
  };

  componentDidMount() {
    LanguageStore.listen(this.onLanguageChange);
  }

  componentWillUnmount() {
    LanguageStore.unlisten(this.onLanguageChange);
  }

  onLanguageChange(state) {
    this.setState(state);
  }
}

export default ComponentWrapper;
