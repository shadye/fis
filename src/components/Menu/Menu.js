import React, {Component} from 'react';
import Wrapper from '../ComponentWrapper';
import classNames from 'classnames';
import styles from './Menu.css';
import withStyles from '../../decorators/withStyles';
import LanguageStore from '../../stores/LanguageStore';
import Link from '../../utils/Link';

@withStyles(styles)
class Menu extends Wrapper {

  constructor(props) {
    super(props);
  }

  render() {
    const content = this.state.content;
    /*let buttons = [];
    for (let key in content) {
      if(content.hasOwnProperty(key)) {
        buttons.push(
          <div className="Menu-button">
            <span className="Menu-button_text">
              {content[key]}
            </span>
          </div>
        )
      }
    }*/

    return (
      <div className={classNames(this.props.className, 'Menu')}>
        <div className="Menu-line">
          <div className="Menu-container">
            <a className="Menu-button" href="/" onClick={Link.handleClick}>
              <span className="Menu-button_text">{content.menu.main}</span>
            </a>
            <a className="Menu-button">
              <span className="Menu-button_text">{content.menu.tutors}</span>
            </a>
            <a className="Menu-button">
              <span className="Menu-button_text">{content.menu.students}</span>
            </a>
            <a className="Menu-button">
              <span className="Menu-button_text">{content.menu.faq}</span>
            </a>
            <a className="Menu-button">
              <span className="Menu-button_text">{content.menu.rules}</span>
            </a>
            <a className="Menu-button">
              <span className="Menu-button_text">{content.menu.forum}</span>
            </a>
          </div>
        </div>
      </div>
    )
  }
}

export default Menu;
