import React from 'react';
import Wrapper from '../ComponentWrapper';
import classNames from 'classnames';
import styles from './Carousel.css';
import withStyles from '../../decorators/withStyles';
import classnames from 'classnames';

@withStyles(styles)
class Carousel extends Wrapper{

  // static propTypes = {
  //   isRight: React.PropTypes.bool
  // }

  constructor(props) {
    super(props);
  }

  leftClick() {
    this.state.isRight = false;
    this.forceUpdate();
  }

  rightClick() {
    this.state.isRight = true;
    this.forceUpdate();
  }

  render() {
    var sliderContainerClasses = 'Carousel-slideContainer';
    var leftIndicatorClasses = 'Carousel-indicator left active';
    var rightIndicatorClasses = 'Carousel-indicator right';
    if (this.state.isRight) {
      sliderContainerClasses += ' right'
      var leftIndicatorClasses = 'Carousel-indicator left';
      var rightIndicatorClasses = 'Carousel-indicator right active';
    }

    return(
      <div className={classNames(this.props.className, 'Carousel')}>
        <div className="Carousel-container">
          <div className="Carousel-controlLeft" onClick={this.leftClick.bind(this)}>
            <span className="Carousel-controlLeft Carousel-controlLine Carousel-controlLine-left"></span>
            <span className="Carousel-controlLeft Carousel-controlLine Carousel-controlLine-right"></span>
          </div>
          <div className="Carousel-controlRight" onClick={this.rightClick.bind(this)}>
          <span className="Carousel-controlRight Carousel-controlLine Carousel-controlLine-left"></span>
          <span className="Carousel-controlRight Carousel-controlLine Carousel-controlLine-right"></span>
          </div>
          <div className={sliderContainerClasses}>
            <img className="Carousel-slide" src={require('./trai1200.png')} width="1200" height="440" alt=""/>
            <img className="Carousel-slide" src={require('./edu1200.png')} width="1200" height="440" alt=""/>
          </div>
          <div className="Carousel-indicatorsContainer">
            <span className={leftIndicatorClasses} onClick={this.leftClick.bind(this)}>•</span>
            <span className={rightIndicatorClasses} onClick={this.rightClick.bind(this)}>•</span>
          </div>
        </div>
      </div>
    );

    // return(
    //   <div className="gallery items-2">
    //     <div id="item-1" className="control-operator"></div>
    //     <div id="item-2" className="control-operator"></div>
    //
    //     <figure className="item">
    //       <img src={require('./trai1200.png')} width="1200" height="440" alt=""/>
    //     </figure>
    //
    //     <figure className="item">
    //       <img src={require('./edu1200.png')} width="1200" height="440" alt=""/>
    //     </figure>
    //
    //     <div className="controls">
    //       <a href="#item1" className="control-button">•</a>
    //       <a href="#item2" className="control-button">•</a>
    //     </div>
    //   </div>
    // );
  }
}

export default Carousel;
