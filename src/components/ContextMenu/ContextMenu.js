import React from 'react';
import Wrapper from '../ComponentWrapper';
import styles from './ContextMenu.css';
import withStyles from '../../decorators/withStyles';
import Link from '../../utils/Link';
import classNames from 'classnames';
import AuthActions from '../../actions/AuthActions';

@withStyles(styles)
class ContextMenu extends Wrapper {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={classNames(this.props.className, 'ContextMenu')} width={this.props.width}>
        <div className="ContextMenu-item">Профиль</div>
        <div className="ContextMenu-item" onClick={AuthActions.logout}>Выход</div>
      </div>
    )
  }

}

export default ContextMenu;
