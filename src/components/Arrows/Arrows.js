import React from 'react';
import classNames from 'classnames';
import styles from './Arrows.css';
import withStyles from '../../decorators/withStyles';

class Arrows extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let self = this;

    return (
      <div className={classNames(this.props.className, 'Arrows-container')}>
        <div className="Arrows-arrows">
          <i className="Arrows-arrows_left"></i>
          <i className="Arrows-arrows_right"></i>
        </div>
          {this.props.insert}
      </div>
    )
}
}

export default withStyles(styles)(Arrows);
