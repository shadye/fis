let en = {
  logo: {
    description: "First international internet-school"
  },
  social: {
    description: "We on soc. networks:"
  },
  login: {
    placeholders: {
      user: "Phone or e-mail",
      pass: "Password"
    },
    links: {
      forgotPass: "Forgot the password?",
      registration: "Registration"
    }
  },
  user: {
    greeting: "Hello, ",
    messages: "New messages: "
  },
  menu: {
    main: "Home",
    tutors: "Tutors",
    students: "Students",
    faq: "F.A.Q.",
    rules: "Rules",
    forum: "Forum"
  },
  footer: {
    links: {
      main: "Home",
      tutors: "Tutors",
      students: "Students",
      faq: "F.A.Q.",
      rules: "Rules",
      forum: "Forum"
    },
    copyright: "2015 FIS. All rights reserved.",
    description: "When copying materials the link is obligatory."
  },
  teasers: {
    logo: "What is FIS?",
    education: "Training without leaving the house",
    tests: "Preparation for examinations",
    training: "Training abroad",
    dialogs: "Communication at forums",
    friends: "Friends worldwide",
    content: "Wide choice of useful content"
  },
  sections: {
    logo: "Popular sections",
    section: {
      logo: "English",
      description: "English language"
    }
  },
  register: {
    header: "Registration",
    inputs: {
      name: {
        "label": "Full name",
        "type": "text",
        "pattern": "([А-ЯЁ][а-яё]+[\\-\\s]?){3,}"
      },
      gender: {
        "label": "Gender",
        "type": "radio",
        "values": ["мужской", "женский"]
      },
      address: {
        "label": "Address",
        "type": "text",
        "pattern": "([А-ЯЁа-яё0-9]+[\\,\\s\\-]+){1,}"
      },
      login: {
        "label": "Login",
        "type": "text",
        "pattern": "[A-Za-z]{6,20}"
      },
      status: {
        "label": "Status",
        "type": "checkbox",
        "values": [
          "Репетитор", "Студент"
        ]
      },
      email: {
        "label": "E-mail",
        "type": "email",
        "pattern": "^(([a-zA-Z]|[0-9])|([-]|[_]|[.]))+[@](([a-zA-Z0-9])|([-])){2,63}[.](([a-zA-Z0-9]){2,63})+$"
      },
      password: {
        "label": "Password",
        "type": "password",
        "pattern": "^((?=\\S*?[A-Z])(?=\\S*?[a-z])(?=\\S*?[0-9]).{6,})\\S$"
      },
      passwordAgain: {
        "label": "Repeat password",
        "type": "password",
        "pattern": "^((?=\\S*?[A-Z])(?=\\S*?[a-z])(?=\\S*?[0-9]).{6,})\\S$"
      }
    },
    "submit": "Submit"
  }
};

export default en;
