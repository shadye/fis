let ru = {
  logo: {
    description: "Первая международная интернет-школа"
  },
  social: {
    description: "Мы в соц. сетях:"
  },
  login: {
    placeholders: {
      user: "Телефон или e-mail",
      pass: "Пароль"
    },
    links: {
      forgotPass: "Забыли пароль?",
      registration: "Регистрация"
    }
  },
  user: {
    greeting: "Здравствуйте, ",
    messages: "Новых сообщений: "
  },
  menu: {
    main: "Главная",
    tutors: "Репетиторы",
    students: "Студенты",
    faq: "Вопросы и ответы",
    rules: "Правила",
    forum: "Форум"
  },
  footer: {
    links: {
      main: "Главная",
      tutors: "Репетиторы",
      students: "Студенты",
      faq: "Вопросы и ответы",
      rules: "Правила",
      forum: "Форум"
    },
    copyright: "2015 FIS. Все права защищены.",
    description: "При копировании материалов ссылка обязательна."
  },
  teasers: {
    logo: "Что такое FIS?",
    education: "Обучение не выходя из дома",
    tests: "Подготовка к экзаменам",
    training: "Стажировка за рубежом",
    dialogs: "Общение на форумах",
    friends: "Друзья по всему миру",
    content: "Большой выбор полезного контента"
  },
  sections: {
    logo: "Популярные разделы",
    section: {
      logo: "English",
      description: "Английский язык"
    }
  },
  register: {
    header: "Регистрация",
    inputs: {
      name: {
        "label": "ФИО",
        "type": "text",
        "pattern": "([А-ЯЁ][а-яё]+[\\-\\s]?){3,}",
        "placeholder": "Иванов Иван Иванович"
      },
      gender: {
        "label": "Пол",
        "type": "radio",
        "values": ["мужской", "женский"]
      },
      address: {
        "label": "Адрес",
        "type": "text",
        "pattern": "([А-ЯЁа-яё0-9]+[\\/\\,\\s\\-]?+){1,}",
        "placeholder": "г Москва, Тверская, 17"
      },
      login: {
        "label": "Логин",
        "type": "text",
        "pattern": "[A-Za-z]{6,20}",
        "placeholder": "ivanovII"
      },
      status: {
        "label": "Статус",
        "type": "checkbox",
        "values": [
          "Репетитор", "Студент"
        ]
      },
      email: {
        "label": "E-mail",
        "type": "email",
        "pattern": "^(([a-zA-Z]|[0-9])|([-]|[_]|[.]))+[@](([a-zA-Z0-9])|([-])){2,63}[.](([a-zA-Z0-9]){2,63})+$",
        "placeholder": "ivanov@domain.com"
      },
      password: {
        "label": "Пароль",
        "type": "password",
        "pattern": "^((?=\\S*?[A-Z])(?=\\S*?[a-z])(?=\\S*?[0-9]).{6,})\\S$",
        "placeholder": "6 символов, включает 1 букву нижнего регистра и цифру"
      },
      passwordAgain: {
        "label": "Повтор пароля",
        "type": "password",
        "pattern": "^((?=\\S*?[A-Z])(?=\\S*?[a-z])(?=\\S*?[0-9]).{6,})\\S$",
        "placeholder": "Идентичен предыдущему полю"
      }
    },
    "submit": "Зарегистрироваться"
  }
};

export default ru;
